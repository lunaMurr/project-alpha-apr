from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm, TaskForm
from tasks.models import Task


@login_required
def list_projects(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {"list_projects": list_projects}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    show_project = get_object_or_404(Project, id=id)
    context = {"show_project": show_project}
    return render(request, "projects/detail.html", context)


@login_required
def show_my_tasks(request):
    show_my_tasks = Task.objects.filter(assignee=request.user)
    context = {"show_my_tasks": show_my_tasks}
    return render(request, "projects/my_tasks.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.purchaser = request.user
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "post_form": form,
    }
    return render(request, "projects/create.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            tasks = form.save(False)
            tasks.purchaser = request.user
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "post_form": form,
    }
    return render(request, "projects/create_task.html", context)
